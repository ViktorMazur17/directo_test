﻿using Microsoft.AspNetCore.Mvc;

namespace DirectoTest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PageNumbersController : ControllerBase
    {

        public PageNumbersController(){}

        [HttpGet]
        [Route("{count}")]
        public string Get(int count)
        {
            if (count < 1)
            {
                return "Invalid number of pages. Please provide positive integer!";
            }
            
            var slotsPerPaper = 8;
            var paperCount = (count + slotsPerPaper - 1) / slotsPerPaper;
            var slotCount = paperCount * 8;

            var result = new int[slotCount];

            for (var i = 0; i < paperCount; i++)
            {
                var paperModifier = (i * 8);
                var halfPaperModifier = paperModifier / 2;

                for (var j = 0; j <= 7; j++)
                {
                    switch (j)
                    {
                        case 0: result[j + paperModifier] = ValueOrEmptyPage(slotCount - halfPaperModifier, count); break;
                        case 1: result[j + paperModifier] = ValueOrEmptyPage(1 + halfPaperModifier, count); break;
                        case 2: result[j + paperModifier] = ValueOrEmptyPage(slotCount - 2 - halfPaperModifier, count); break;
                        case 3: result[j + paperModifier] = ValueOrEmptyPage(3 + halfPaperModifier, count); break;
                        case 4: result[j + paperModifier] = ValueOrEmptyPage(2 + halfPaperModifier, count); break;
                        case 5: result[j + paperModifier] = ValueOrEmptyPage(slotCount - 1 - halfPaperModifier, count); break;
                        case 6: result[j + paperModifier] = ValueOrEmptyPage(4 + halfPaperModifier, count); break;
                        case 7: result[j + paperModifier] = ValueOrEmptyPage(slotCount - 3 - halfPaperModifier, count); break;
                    }
                }
            }

            return string.Join(",", result);
        }

        private int ValueOrEmptyPage(int value, int count)
        {
            return value <= count ? value : -1;
        }
    }
}
